import {Button, Dialog, DialogContent, DialogTitle, TextField, Typography} from "@mui/material";
import {Dispatch, SetStateAction, useState} from "react";
import axios from "axios";


type RegisterDialogProps = {
  open: boolean,
  setOpen: Dispatch<SetStateAction<boolean>>,

}

type UserData = {
  first_name: string,
  last_name: string,
  email: string,
  password: string

}

export const RegisterDialog = ({open, setOpen}: RegisterDialogProps) => {
  const [dataForm, setDataForm] = useState<UserData>({
    first_name: '',
    last_name: '',
    email: '',
    password: ''
  })

  const sendUserData = () => {

    axios.post('http://localhost:4001/api/user/register', dataForm)
      .then((response) => {
        console.log('Response:', response.data);
        setOpen(false)
      })
      .catch((error) => {
        console.error('Error:', error.message);
      });

  }


  return (
    <>
      <Dialog onClose={() => {
        setOpen(false)
      }} open={open}>
        <DialogTitle>
          <Typography variant={'h4'}>
            Register
          </Typography>
        </DialogTitle>
        <DialogContent className={'flex flex-col m-3 gap-4'}>


          <TextField className={'m-3'} label={'prenom'} name={'first_name'} value={dataForm.first_name}
                     onChange={(e) => {
                       setDataForm({
                           ...dataForm,
                           first_name:
                           e.target.value
                         }
                       )
                     }}>

          </TextField>


          <TextField className={'m-3'} label={'nom'} name={'last_name'} value={dataForm.last_name} onChange={(e) => {
            setDataForm({
                ...dataForm,
                last_name:
                e.target.value
              }
            )
          }}>

          </TextField>


          <TextField className={'m-3'} label={'email'} name={'email'} value={dataForm.email} onChange={(e) => {
            setDataForm({
                ...dataForm,
                email:
                e.target.value
              }
            )
          }}>

          </TextField>


          <TextField className={'m-3'} label={'mot de passe'} name={'password'} value={dataForm.password}
                     onChange={(e) => {
                       setDataForm({
                           ...dataForm,
                           password:
                           e.target.value
                         }
                       )
                     }}>

          </TextField>

          <div>
            <Button onClick={() => {
              sendUserData()
            }}>Se connecter</Button>

            <Button onClick={() => {
              setOpen(false)
            }}>Fermer</Button>
          </div>
        </DialogContent>
      </Dialog>

    </>
  )
}
