'use client'

import {Card} from "@mui/material";

type MoonPhaseProps = {
  header: string,
  textInfo: string,
  footer: string
}

export default function MoonPhasesCard({header, textInfo, footer}: MoonPhaseProps) {

  return (<>

    <Card>
      {header}
    </Card>
    <Card>
      {textInfo}
    </Card>
    <Card>
      {footer}
    </Card>
  </>)

}
