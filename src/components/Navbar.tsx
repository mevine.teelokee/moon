'use client'
import * as React from 'react'
import {useState} from 'react'
import Link from "next/link";
import Logo from "../../public/moon-logo.png"
import Image from 'next/image'
import axios from "axios";
import {RegisterDialog} from "@/components/RegisterDialog";

export function PrimarySearchAppBar() {

  const [dialogState, setDialogState] = useState<boolean>(false)

  const fetchAllUser = async () => {
    try {
      const response = await axios.get('http://localhost:4001/api/user/all');
      console.log('Response:', response.data);
    } catch (error: any) {
      console.error('Error:', error.message);
    }
  }


  return (
    <nav className="flex items-center justify-between p-5 pb-2 lg:px-8 border-b-2" aria-label="Global">
      <div className="flex lg:flex-1">
        <Link href="/" className="-m-1.5 p-1.5 flex flex-row">
          <Image className="h-8 w-auto" src={Logo} alt=""/>
          <span className="flex lg:flex-1 pl-3 items-center ">Moon Phases</span>
        </Link>
      </div>
      <div className="flex lg:hidden">
        <button type="button"
                className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700">
          <span className="sr-only">Open main menu</span>
          <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor"
               aria-hidden="true">
            <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"/>
          </svg>
        </button>
      </div>
      <div className="hidden lg:flex lg:gap-x-12">
        <Link href="/" className="text-sm font-semibold leading-6 text-gray-900">Accueil</Link>
        <Link href="/my-daily-space" className="text-sm font-semibold leading-6 text-gray-900">Journal de bord</Link>
        <Link href="/moon-wiki" className="text-sm font-semibold leading-6 text-gray-900">Wiki Lunaire</Link>
        <Link href="/my-stats" className="text-sm font-semibold leading-6 text-gray-900">Statistiques</Link>
      </div>
      <div className="hidden lg:flex lg:flex-1 lg:justify-end">
        <button onClick={() => {
          setDialogState(true)
        }} className="text-sm font-semibold leading-6 text-gray-900">Se connecter <span
          aria-hidden="true">&rarr;</span></button>
      </div>

      <RegisterDialog open={dialogState} setOpen={setDialogState}/>

    </nav>

  )
}
