'use client'
import React from "react";
import MoonPhasesCard from "@/components/MoonPhasesCard";
// import MoonANimationTest from "@/components/MoonANimationTest";

export default function Home() {
  const moonPhasesData = [{header: 'toto', text: 'text', footer: 'footer1'}, {
    header: 'Nouvelle lune',
    text: ' Thèmes du moment : renouveau, rêver, repos\n' +
      '        Rituels : Méditation pour poser ses intentions, relâcher ce qui n\'est plus aligné avec soi-même\n' +
      '        Planter une graine qui va germer tout au long du cycle\n' +
      '        Tirage de cartes d\'oracle\n' +
      '        La graine est plantée',
    footer: 'IMAGE'
  }, {header: 'toto1', text: 'text', footer: 'footer1'}, {
    header: 'toto2',
    text: 'text',
    footer: 'footer1'
  }, {header: 'toto3', text: 'text', footer: 'footer1'}, {
    header: 'toto4',
    text: 'text',
    footer: 'footer1'
  }, {header: 'toto5', text: 'text', footer: 'footer1'}]


  return (
    <>
      {/*<MoonANimationTest/>*/}

      {moonPhasesData.map(it => {

        return (
          <MoonPhasesCard key={it.header} header={it.header} textInfo={it.text} footer={it.footer}/>
        )

      })}


      {/*<div className={"text-2xl"}>*/}
      {/*  Introduction*/}
      {/*  La lune suit des cycles d'environ 29 jours. Pendant cette période, elle fait le tour de la Terre et est éclairée*/}
      {/*  différemment par la lumière du soleil selon sa position et celle de la terre.*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div> 8 phases</div>*/}
      {/*<br/>*/}
      {/*<div> Nouvelle lune*/}
      {/*  Image*/}
      {/*  Thèmes du moment : renouveau, rêver, repos*/}
      {/*  Rituels : Méditation pour poser ses intentions, relâcher ce qui n'est plus aligné avec soi-même*/}
      {/*  Planter une graine qui va germer tout au long du cycle*/}
      {/*  Tirage de cartes d'oracle*/}
      {/*  La graine est plantée*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div> Premier croissant*/}
      {/*  Image*/}
      {/*  Thèmes du moment: manifester, visualiser, créativité*/}
      {/*  Rituels : explorer son intention en la visualisant et en pensant aux manières de la réaliser, préparer les*/}
      {/*  actions*/}
      {/*  La germe commence à sortir doucement de la terre*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div> Premier quartier*/}
      {/*  Image*/}
      {/*  Thèmes du moment : agir, nourrir son intention, force*/}
      {/*  Rituels : agir, être active dans la réalisation de son intention, s'ouvrir aux opportunités*/}
      {/*  La plante grandit*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div> Gibbeuse croissante*/}
      {/*  Image*/}
      {/*  Thèmes du moment : détermination, ajustement*/}
      {/*  Rituels : continuer à agir et ajuster ses actions en fonction de ce qui émerge, noter les difficultés qui*/}
      {/*  apparaissent*/}
      {/*  La plante a poussé, ses bourgeons sont prêts à éclore*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div> Gibbeuse croissante*/}
      {/*  Image*/}
      {/*  Thèmes du moment : détermination, ajustement*/}
      {/*  Rituels : continuer à agir et ajuster ses actions en fonction de ce qui émerge, noter les difficultés qui*/}
      {/*  apparaissent*/}
      {/*  La plante a poussé, ses bourgeons sont prêts à éclore*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div> Pleine lune*/}
      {/*  Image*/}
      {/*  Thèmes du moment : illumination*/}
      {/*  Rituels : célébrer ses réussites, méditation pour comprendre ce que la lumière de la pleine lune illumine sur*/}
      {/*  son*/}
      {/*  intention*/}
      {/*  Tirage de cartes d'oracle*/}

      {/*  Les fleurs éclosent et les fruits arrivent.*/}
      {/*</div>*/}
      {/*<br/>*/}
      {/*<div>*/}
      {/*  Gibbeuse décroissante*/}
      {/*  Image*/}
      {/*  Thèmes du moment : récolte, contemplation*/}
      {/*  Rituels : remercier et trier, commencer à prendre conscience de ce que le cycle lunaire nous a enseigné*/}

      {/*  Les fruits sont récoltés*/}
      {/*</div>*/}
      {/*<br/>*/}

      {/*<div>*/}
      {/*  Dernier quartier*/}
      {/*  Image*/}
      {/*  Thèmes du moment : descente intérieure, sagesse*/}
      {/*  Rituels : tirer des leçons du cycle lunaire, commencer à se tourner vers l'intérieur*/}
      {/*  Les fruits et la plante se déposent sur terre.*/}
      {/*</div>*/}
      {/*<br/>*/}

      {/*<div>*/}
      {/*  Dernier croissant*/}
      {/*  Image*/}
      {/*  Thèmes du moment : repos, relâchement*/}
      {/*  Rituels : se tourner vers l'intérieur, identifier ce qui n'est plus aligné et qui sera relâché à la nouvelle*/}
      {/*  lune,*/}
      {/*  se connecter à son intuition, prendre du temps pour soi*/}
      {/*  La plante retourne à la terre et devient compost.*/}
      {/*</div>*/}
      {/*<br/>*/}

    </>
  )
}

