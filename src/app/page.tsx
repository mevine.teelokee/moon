'use client'
import MoonAnimation from "@/components/MoonAnimation";
import React from "react";
import {useEffect} from 'react';

export default function Home() {
  return (<>
      <div className=" h-screen flex items-center">
        <div className="container mx-auto text-center">
          <h1 className="text-4xl font-bold mb-4">Commencez votre voyage</h1>
          <p className="text-lg mb-8">Discover amazing things with us</p>
          <a href="#"
             className="bg-white text-blue-500 px-6 py-3 rounded-full font-bold hover:bg-blue-400 hover:text-white transition duration-300">Get
            Started</a>
        </div>
        {/*<MoonAnimation/>*/}
      </div>

      <div className="container mx-auto mt-8">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8">
          <div className="p-4 bg-white rounded shadow">
            <h2 className="text-2xl font-bold mb-4">About Us</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.</p>
          </div>

          <div className="p-4 bg-white rounded shadow">
            <h2 className="text-2xl font-bold mb-4">Our Services</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.</p>
          </div>
        </div>
      </div>


    </>
  )
}
